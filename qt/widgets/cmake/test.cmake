#
# test.cmake
# CMake makefile include for the A.Wise Toolbox Qt Widgets library tests.
#

#
# This file is part of awisetoolbox.
# Copyright (C) 2008-2018 by Alan Wise <awisesoftware@gmail.com>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


# Options.
OPTION(OPTION_AWISETOOLBOX_QTWIDGETS_BUILDTESTS
    "Build the ${DISPLAYNAME} tests." OFF)

IF(OPTION_AWISETOOLBOX_QTWIDGETS_BUILDTESTS)

  #
  # Configuration
  #

  # Need Qt4.
  IF(NOT QT4_FOUND)
    FIND_PACKAGE(Qt4 REQUIRED)
    INCLUDE(${QT_USE_FILE})
  ENDIF()


  #
  # Include paths
  #


  #
  # Sources
  #


  #
  # Binaries
  #


  #
  # Subdirectories
  #


  #
  # Installation
  #


ENDIF()


#
# test.cmake
#
