#
# documentation.cmake
# CMake makefile include for the A.Wise Toolbox Qt Designer plugin documentation.
#

#
# This file is part of awisetoolbox.
# Copyright (C) 2008-2018 by Alan Wise <awisesoftware@gmail.com>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


# Options.
INCLUDE(CMakeDependentOption)
CMAKE_DEPENDENT_OPTION(OPTION_AWISETOOLBOX_QTDESIGNER_BUILDDOCUMENTATION
    "Build the ${DISPLAYNAME} documentation." OFF G_RELEASEFLAG OFF)

IF(OPTION_AWISETOOLBOX_QTDESIGNER_BUILDDOCUMENTATION AND G_RELEASEFLAG)

  #
  # Configuration
  #

  # Need Doxygen.
  IF(NOT DOXYGEN_FOUND)
    FIND_PACKAGE(Doxygen REQUIRED)
  ENDIF()

  # Create the Doxygen file.
  CONFIGURE_FILE(
      "${CMAKE_DIR}/Doxyfile.in"
      "${CMAKE_CURRENT_BINARY_DIR}/Doxyfile-${LIBRARYNAME}")


  #
  # Include paths
  #


  #
  # Sources
  #


  #
  # Binaries
  #

  # Build the documentation.
  ADD_CUSTOM_TARGET(
      doc-${LIBRARYNAME} ALL
      COMMAND ${DOXYGEN_EXECUTABLE}
      "${CMAKE_CURRENT_BINARY_DIR}/Doxyfile-${LIBRARYNAME}"
      SOURCES "${CMAKE_CURRENT_BINARY_DIR}/Doxyfile-${LIBRARYNAME}")


  #
  # Subdirectories
  #


  #
  # Installation
  #


ENDIF()


#
# documentation.cmake
#
