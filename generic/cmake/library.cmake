#
# library.cmake
# CMake makefile include for the A.Wise Toolbox Generic library.
#

#
# This file is part of awisetoolbox.
# Copyright (C) 2008-2018 by Alan Wise <awisesoftware@gmail.com>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


# Options.
OPTION(OPTION_AWISETOOLBOX_GENERIC_BUILDLIBRARY
    "Build the ${DISPLAYNAME} library." ON)

IF(OPTION_AWISETOOLBOX_GENERIC_BUILDLIBRARY)

  #
  # Configuration
  #

  # Additional directory for 'Find' scripts.
  SET(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH}
      "${CMAKE_DIR}/Modules/")

  # Optional MHash.
  FIND_PACKAGE(MHASH)
  IF(MHASH_FOUND)
    ADD_DEFINITIONS(${G_DEFINEPREFIX}BUILD_MHASH)
  ELSE()
    MESSAGE(WARNING "\nMhash library not found! Checksum module will not be built.")
  ENDIF()


  #
  # Include paths
  #


  #
  # Sources
  #

  SET(SOURCES
      "${SOURCES_DIR}/checksum.c"
      "${SOURCES_DIR}/debuglog.c"
      "${SOURCES_DIR}/endianness.c"
      "${SOURCES_DIR}/file.c"
      "${SOURCES_DIR}/messagelog.c"
      "${SOURCES_DIR}/msvc.c"
      "${SOURCES_DIR}/pathnamelist.c"
      "${SOURCES_DIR}/utility.c"
      "${SOURCES_DIR}/versionstring.c")
  SET(FILES ${SOURCES})


  #
  # Binaries
  #

  # Build the library.
  ADD_LIBRARY(
      ${LIBRARYNAME} STATIC
      ${FILES}
      ${MHASH_LIBRARIES})


  #
  # Subdirectories
  #


  #
  # Installation
  #


ENDIF()


#
# library.cmake
#
