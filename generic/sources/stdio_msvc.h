/*
** This file is part of awisetoolbox.
** Copyright (C) 2008-2018 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
*** \file stdio_msvc.h
*** \brief Microsoft omissions from "stdio.h".
*** \details Function prototypes that are not native to Microsoft SDKs,
***   but are needed by awisetoolbox or other programs.
**/


#if       defined(_WIN32)

#if       !defined(STDIO_MSVC_H)
#define   STDIO_MSVC_H


/****
*****
***** INCLUDES
*****
****/

#include  <stdarg.h>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/

#if       defined(__cplusplus)
extern "C" {
#endif    /* defined(__cplusplus) */

int asprintf(char **ppString,const char *pFormat,...);
int vasprintf(char **ppString,const char *pFormat,va_list pList);

#if       defined(__cplusplus)
}
#endif    /* defined(__cplusplus) */


#endif    /* !defined(STDIO_MSVC_H) */

#endif    /* defined(_WIN32) */
