/**
*** \file endianness.h
*** \brief Endian testing and conversion utilities.
*** \details Utilities for testing and converting endianness.
**/

/*
** This file is part of awisetoolbox.
** Copyright (C) 2016-2018 by Alan Wise <awisesoftware@gmail.com>.
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef   ENDIANNESS_H
/** Identifier for endianness.h. **/
#define   ENDIANNESS_H


/****
*****
***** INCLUDES
*****
****/

#include  "sysdefs.h"

#include  <stdint.h>


/****
*****
***** DEFINES
*****
****/


/****
*****
***** DATA TYPES
*****
****/


/****
*****
***** DATA
*****
****/


/****
*****
***** VARIABLES
*****
****/


/****
*****
***** PROTOTYPES
*****
****/

#ifdef  __cplusplus
extern "C" {
#endif  /* __cplusplus */

/**
*** \brief Is processor big endian.
*** \details Checks if the processor is big endian.
*** \retval BOOLEAN_FALSE - Processor is little endian.
*** \retval BOOLEAN_TRUE - Processor is big endian.
**/
BOOLEAN_T IsBigEndian(void);

/**
*** \brief Is processor little endian.
*** \details Checks if the processor is little endian.
*** \retval BOOLEAN_FALSE - Processor is big endian.
*** \retval BOOLEAN_TRUE - Processor is little endian.
**/
BOOLEAN_T IsLittleEndian(void);

/**
*** \brief Swap bytes.
*** \details Swaps bytes in a word (2 bytes).
*** \param Value Value to byte swap.
*** \returns Byte swapped value.
**/
uint16_t Swap16(uint16_t Value);

/**
*** \brief Swap words.
*** \details Swaps words (and bytes) in a dword (4 bytes).
*** \param Value Value to word swap.
*** \returns Word swapped value.
**/
uint32_t Swap32(uint32_t Value);

/**
*** \brief Swap dwords.
*** \details Swaps dwords (and words and bytes) in a qword (8 bytes).
*** \param Value Value to dword swap.
*** \returns Dword swapped value.
**/
uint64_t Swap64(uint64_t Value);

#ifdef  __cplusplus
}
#endif  /* __cplusplus */


#endif    /* ENDIANNESS_H */


/**
*** endianness.h
**/
