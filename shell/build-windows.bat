@echo off

:: Unable to pass environment variables from child process to parent,
::  so the script is inlined.

:: windows-env.bat %1


:: Parameter checking.
if "x86_64"=="" (
  echo
  echo Error: missing architecture.
  exit /b 1
)

:: Set up architecture variable.
setlocal
set Arch=
if "x86_64"=="x86" (
  set Arch=x86
)
if "x86_64"=="x86_64" (
  set Arch=amd64
)

if "%Arch%"=="" (
  echo.
  echo Error: unsupported architecture ^(%1^). Use "x86" or "x86_64".
  exit /b 1
)

call "c:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" %Arch%
if ERRORLEVEL 1 exit /b 1
set PATH=%PATH%;"c:\SoftwareDevelopment\CMake\bin";c:\SoftwareDevelopment\Qt\4.8.6\x86_64\bin
REM set CMAKE_PREFIX_PATH=c:\SoftwareDevelopment\x86_64


:: windows-env.bat

if ERRORLEVEL 1 exit /b 1
call shell\windows-build.bat
