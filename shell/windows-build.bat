@echo off

rmdir /s /q .build 2>1 >nul
mkdir .build
if ERRORLEVEL 1 exit /b 1
cd .build
if ERRORLEVEL 1 exit /b 1
cmake -G "NMake Makefiles" ..
if ERRORLEVEL 1 exit /b 1
nmake /s /nologo
if ERRORLEVEL 1 exit /b 1
cd ..
